###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# This script prepares the environment to run the nightly builds tasks

# Validate the execution environment
if [ -z "$WORKSPACE" ] ; then
  echo 'error: the environment does not look like a Jenkins job ($WORKSPACE not defined)'
  exit 1
fi

if [ ! -r "$WORKSPACE/$(basename ${BASH_SOURCE[0]})" ] ; then
  echo 'error: $WORKSPACE should point to the directory containing this file'
  exit 1
fi

if [ -n "$TMPDIR" -a ! -d "$TMPDIR" ] ; then
  mkdir -p "$TMPDIR"
fi

case $(uname -m) in
  x86_64 ) conda_arch=linux-64 ;;
  aarch64 ) conda_arch=linux-aarch64 ;;
  * ) echo "unsupported arch $(uname -m)" ; exit 1 ;;
esac

if [ $conda_arch = "linux-64" ] ; then
  # add CMT to the path
  export PATH=/cvmfs/lhcb.cern.ch/lib/bin/Linux-x86_64:$PATH
fi

conda_exe=/cvmfs/lhcbdev.cern.ch/conda/miniconda/${conda_arch}/prod/condabin/conda

# use a specific conda environment for old platforms
case "$platform" in
    *slc6* | *slc5* )
        export env_hash=d793e9f5b9f35ede449593454325378b56dfde173d823ea6e978b81e5bcba3af
        ;;
esac

# Check if we have an externally provided conda environment hash
if [ -z "$env_hash" ] ; then
  # otherwise we get the default from LHCbNightlyConf
  curl -L --output list-environments.zip "https://gitlab.cern.ch/lhcb-core/LHCbNightlyConf/-/jobs/artifacts/master/download?job=list-environments"
  export env_hash=$(unzip -p list-environments.zip env-hashes.yaml | awk '/^[^ ]+:/{env=$1} /^  '${conda_arch}':/{if (env == "legacy:") {print $2; exit}}')
  rm -f list-environments.zip
  # make sure we got the hash
  test -n "$env_hash"
fi

# wait up to one hour for the CVMFS directory to appear
echo "$(date -Is) - waiting for /cvmfs/lhcbdev.cern.ch/nightly-environments/$env_hash/bin"
for cnt in $(seq 0 60) ; do
  if [ -r /cvmfs/lhcbdev.cern.ch/nightly-environments/$env_hash/bin ] ; then
    echo "$(date -Is) - found"
    break
  fi
  echo "."
  sleep 60
done
test -r /cvmfs/lhcbdev.cern.ch/nightly-environments/$env_hash/bin
echo "$(date -Is) - waited for $cnt minutes"

# activate the requested environment
eval "$($conda_exe shell.posix activate /cvmfs/lhcbdev.cern.ch/nightly-environments/$env_hash)"
PS1="(cvmfs env $(cut -b1-10 <<< $env_hash)) "

# see if we need to apply some overrides to the environment
if [ -n "$JENKINS_OVERRIDE_PIP_REQUIREMENTS" ] ; then
  rm -rf tmp/venv
  python -m venv --system-site-packages tmp/venv
  . tmp/venv/bin/activate
  pip install $JENKINS_OVERRIDE_PIP_REQUIREMENTS
fi

# make sure we can patch the cacert file
rm -rf $WORKSPACE/certifi
cp -r $(dirname $(python -m certifi)) $WORKSPACE
export PYTHONPATH=$WORKSPACE${PYTHONPATH+:$PYTHONPATH}

# make sure we use the right cacert file in all cases
export SSL_CERT_FILE=$(python -m certifi)

# Patch the cacert file with CERN certificates
cat $WORKSPACE/cern_ca_bundle.crt >> $SSL_CERT_FILE
