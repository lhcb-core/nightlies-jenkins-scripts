#!/bin/bash -e
(
    echo "# CERN CA certificates bundle"
    curl -q "https://ca.cern.ch/cafiles/certificates/CERN%20Root%20Certification%20Authority%202.crt" | openssl x509 -inform DER -outform PEM
    curl -q "https://ca.cern.ch/cafiles/certificates/CERN%20Certification%20Authority.crt"
    curl -q "https://ca.cern.ch/cafiles/certificates/CERN%20Certification%20Authority(1).crt"
    curl -q "https://ca.cern.ch/cafiles/certificates/CERN%20Certification%20Authority(2).crt"
    curl -q "https://ca.cern.ch/cafiles/certificates/CERN%20Grid%20Certification%20Authority(1).crt"
) > cern_ca_bundle.crt
dos2unix cern_ca_bundle.crt > /dev/null 2>&1 || true
