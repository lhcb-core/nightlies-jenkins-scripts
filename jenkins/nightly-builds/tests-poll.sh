#!/bin/bash -e
###############################################################################
# (c) Copyright 2013-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

. $(dirname $0)/../utils.sh

set_common

rm -f test-*.txt

for flavour in ${flavours} ; do
  lbn-test-poll --debug --submit --flavour ${flavour}
done

# Redirect Allen tests for CUDA platforms to dedicated nodes
for test_file in $(ls | grep '^test-.*txt$'); do
  if grep -q '^project=\(Allen\|MooreOnline\)$' $test_file && grep -q '^platform=.*cuda' $test_file ; then
    sed -i 's/^tests_node=.*/tests_node=cuda-tests/' $test_file
  fi
done
