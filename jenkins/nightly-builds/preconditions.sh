#!/bin/bash
###############################################################################
# (c) Copyright 2013-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

. $(dirname $0)/../utils.sh

platform_save=$platform
unset platform

set_common

export platform=$platform_save
unset platform_save
# this is often required by preconditions definitions
export BINARY_TAG=$platform

execute_preconditions "${slot}.${slot_build_id}"
