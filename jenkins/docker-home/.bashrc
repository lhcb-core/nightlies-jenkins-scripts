# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

PS1="[container \W]\$ "

# User specific aliases and functions
