###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Check that there are no problems reading from CVMFS
if ! ls -d /cvmfs/{lhcb,lhcbdev,lhcb-condb,sft,sft-nightlies,cernvm-prod,projects}.cern.ch/. > /dev/null ; then
    echo "ERROR: some CVMFS mount points cannot be accessed"
    # Send a mail
    if which mailx >& /dev/null ; then
        (
            echo "Node URL: https://jenkins-lhcb-nightlies.web.cern.ch/computer/${NODE_NAME}"
            echo "Build URL: ${BUILD_URL}"
            ls -d /cvmfs/{lhcb,lhcbdev,lhcb-condb,sft,sft-nightlies,cernvm-prod,projects}.cern.ch/. 2>&1 || true
        ) | mailx -r "LHCb Jenkins <noreply@cern.ch>" \
                  -s "error: $(hostname): some CVMFS mount points cannot be accessed" \
                  lhcb-core-soft-alarms@cern.ch
    fi
    # try to disable the node
    if false && which auth-get-sso-cookie >&/dev/null && test -r ${HOME}/private/lhcbsoft.pwd ; then
        echo "Flagging the node as offline"
        cookie_file=$(mktemp)
        kinit -k -t $PRIVATE_DIR/lhcbsoft.keytab lhcbsoft@CERN.CH
        auth-get-sso-cookie -u https://jenkins-lhcb-nightlies.web.cern.ch/securityRealm/commenceLogin -o ${cookie_file}
        curl --silent -L -b ${cookie_file} -X POST "https://jenkins-lhcb-nightlies.web.cern.ch/computer/${NODE_NAME}/changeOfflineCause?offlineMessage=some%20CVMFS%20mount%20points%20cannot%20be%20accessed" > /dev/null
        rm -f ${cookie_file}
    else
        echo "Rebooting"
        sudo reboot
    fi
    exit 1
else
    echo "CVMFS mount points are accessible"
fi
