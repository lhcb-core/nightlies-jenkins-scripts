#!/usr/bin/env bash
###############################################################################
# (c) Copyright 2013-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

export buildid="${slot}.${slot_build_id}"
export artifactsdir="${ARTIFACTS_DIR}"

export lhcbpr_api_url=${lhcbpr_api_url:-"https://lblhcbpr.cern.ch/api"}

lhcbpr_check_ssl_cmd=$( [ ! -z ${NOSQL+x} ] && echo "--check-ssl")
echo "Build file to run test"
lbpr-get-command --url $lhcbpr_api_url $lhcbpr_check_ssl_cmd -o runtest.sh  $project $buildid $platform  $testgroup $testenv $config_file || exit $?
echo
echo "Now running the test"
mycount=${count:-1}
echo "Will run the test $mycount time(s)"
for i in `seq 1 $mycount`; do
    sh runtest.sh $i
    if [[ $? -ne 0 ]]; then
	break
    fi
done
